import * as isPossible from '../src/move-validation'
import * as pieces from '../src/piece'
import { Chessboard, createEmptyChessboard, putPiece } from '../src/chessboard';
import { Position, position } from '../src/position';
import { Move, move } from '../src/movements';

let chessboard : Chessboard;

const positionA4 : Position = position(0, 3) // A4
const positionA5 : Position = position(0, 4) // A5
const positionA6 : Position = position(0, 5) // A6
const positionA7 : Position = position(0, 6) // A7
const positionA8 : Position = position(0, 7) // A8

const positionB1 : Position = position(1, 0) // B1
const positionB2 : Position = position(1, 1) // B2
const positionB5 : Position = position(1, 4) // B5
const positionB6 : Position = position(1, 5) // B6

const positionC2 : Position = position(2, 1) // C2
const positionC3 : Position = position(2, 2) // C3
const positionC4 : Position = position(2, 3) // C4
const positionC5 : Position = position(2, 4) // C5
const positionC6 : Position = position(2, 5) // C6
const positionC7 : Position = position(2, 6) // C7

const positionD2 : Position = position(3, 1) // D2
const positionD3 : Position = position(3, 2) // D3
const positionD4 : Position = position(3, 3) // D4
const positionD5 : Position = position(3, 4) // D5
const positionD6 : Position = position(3, 5) // D6

const positionE1 : Position = position(4, 0) // E1
const positionE2 : Position = position(4, 1) // E2
const positionE3 : Position = position(4, 2) // E3
const positionE4 : Position = position(4, 3) // E4
const positionE5 : Position = position(4, 4) // E5
const positionE6 : Position = position(4, 5) // E6
const positionE7 : Position = position(4, 6) // E7
const positionE8 : Position = position(4, 7) // E8

const positionF2 : Position = position(5, 1) // F2
const positionF3 : Position = position(5, 2) // F3
const positionF4 : Position = position(5, 3) // F4
const positionF5 : Position = position(5, 4) // F5
const positionF6 : Position = position(5, 5) // F6

const positionG3 : Position = position(6, 2) // G3
const positionG4 : Position = position(6, 3) // G4
const positionG5 : Position = position(6, 4) // G5
const positionG6 : Position = position(6, 5) // G6

const positionH1 : Position = position(7, 0) // H1
const positionH4 : Position = position(7, 3) // H4
const positionH7 : Position = position(7, 8) // H7

// Horizontal moves
const moveE4_H4 : Move = move(createEmptyChessboard(), positionE4, positionH4);
const moveE4_A4 : Move = move(createEmptyChessboard(), positionE4, positionA4);

// Vertical moves
const moveE4_E1 : Move = move(createEmptyChessboard(), positionE4, positionE1);
const moveE4_E8 : Move = move(createEmptyChessboard(), positionE4, positionE8);

// Diagonal moves
const moveE4_A8 : Move = move(createEmptyChessboard(), positionE4, positionA8);
const moveE4_B1 : Move = move(createEmptyChessboard(), positionE4, positionB1);
const moveE4_H7 : Move = move(createEmptyChessboard(), positionE4, positionH7);
const moveE4_H1 : Move = move(createEmptyChessboard(), positionE4, positionH1);

// Knight moves
const moveE4_F6 : Move = move(createEmptyChessboard(), positionE4, positionF6);
const moveE4_G5 : Move = move(createEmptyChessboard(), positionE4, positionG5);
const moveE4_F2 : Move = move(createEmptyChessboard(), positionE4, positionF2);
const moveE4_G3 : Move = move(createEmptyChessboard(), positionE4, positionG3);
const moveE4_D2 : Move = move(createEmptyChessboard(), positionE4, positionD2);
const moveE4_C3 : Move = move(createEmptyChessboard(), positionE4, positionC3);
const moveE4_C5 : Move = move(createEmptyChessboard(), positionE4, positionC5);
const moveE4_D6 : Move = move(createEmptyChessboard(), positionE4, positionD6);

// Impossible moves
const moveE4_C7 : Move = move(createEmptyChessboard(), positionE4, positionC7);
const moveE4_B2 : Move = move(createEmptyChessboard(), positionE4, positionB2);
const moveE4_G6 : Move = move(createEmptyChessboard(), positionE4, positionG6);
const moveE4_E6 : Move = move(createEmptyChessboard(), positionE4, positionE6);
const moveE4_G4 : Move = move(createEmptyChessboard(), positionE4, positionG4);

// Promotion moves
const moveE7_E8 : Move = move(createEmptyChessboard(), positionE7, positionE8);
const moveE2_E1 : Move = move(createEmptyChessboard(), positionE2, positionE1);
const moveB5_E8 : Move = move(createEmptyChessboard(), positionB5, positionE8);

describe("Test blackPawnMove()", () => {
    beforeEach( () => {
        chessboard = createEmptyChessboard();
    })

    it("Pawns can move forward", () => {
        putPiece(chessboard, positionA7, pieces.blackPawn);
        let singleForward: Move = {from: positionA7, to: positionA6, isValid: true};
        expect(isPossible.blackPawnMove(chessboard, singleForward)).toBeTruthy();
    });

    it("Pawns cannot move backward", () => {
        putPiece(chessboard, positionA7, pieces.blackPawn);
        let singleForward: Move = {from: positionA7, to: positionA8, isValid: true};
        expect(isPossible.blackPawnMove(chessboard, singleForward)).toBeFalsy();
    });

    it("When in the initial position, paws can move 2 squares forward", () => {
        putPiece(chessboard, positionA7, pieces.blackPawn);
        let doubleForward: Move = {from: positionA7, to: positionA5, isValid: true};
        expect(isPossible.blackPawnMove(chessboard, doubleForward)).toBeTruthy();
    });

    it("When a paws has already moved, it cannot move 2 squares forward", () => {
        putPiece(chessboard, positionC6, pieces.blackPawn);
        let doubleForward: Move = {from: positionC6, to: positionC4, isValid: true}
        expect(isPossible.blackPawnMove(chessboard, doubleForward)).toBeFalsy();
    });

    it("When in the initial position, pawns cannot move 3 squares forward", () => {
        putPiece(chessboard, positionC6, pieces.blackPawn);
        let tripleForward: Move = {from: positionA7, to: positionA4, isValid: true}
        expect(isPossible.blackPawnMove(chessboard, tripleForward)).toBeFalsy();
    });

    it("When in face of another piece, pawns cannot move foreward", () => {
        putPiece(chessboard, positionA6, pieces.whitePawn);
        putPiece(chessboard, positionA7, pieces.blackPawn);
        let singleForward: Move = {from: positionA7, to: positionA6, isValid: true}
        expect(isPossible.blackPawnMove(chessboard, singleForward)).toBeFalsy();
    })

    it("Pawns cannot capture an empty square ", () => {
        putPiece(chessboard, positionA7, pieces.blackPawn);
        let diagonalCapture: Move = {from: positionA7, to: positionB6, isValid: true}
        expect(isPossible.blackPawnMove(chessboard, diagonalCapture)).toBeFalsy();
    });

    it("Pawns cannot capture pieces of the same color", () => {
        putPiece(chessboard, positionA7, pieces.blackPawn);
        putPiece(chessboard, positionB6, pieces.blackKing);

        let diagonalCapture: Move = {from: positionA7, to: positionB6, isValid: true}
        expect(isPossible.blackPawnMove(chessboard, diagonalCapture)).toBeFalsy();
    });

    it("Pawns can capture pieces of a different color", () => {
        putPiece(chessboard, positionA7, pieces.blackPawn);
        putPiece(chessboard, positionB6, pieces.whiteQueen);

        let diagonalCapture: Move = {from: positionA7, to: positionB6, isValid: true}
        expect(isPossible.blackPawnMove(chessboard, diagonalCapture)).toBeTruthy();
	});
});

describe("Test whitePawnMove()", () => {
    beforeEach( () => {
        chessboard = createEmptyChessboard();
    })

    it("When there is a piece in front it, pawns cannot move forward", () => {
        putPiece(chessboard, positionD4, pieces.whitePawn); // White Pawn at D4
        putPiece(chessboard, positionD5, pieces.blackPawn); // Black Pawn at D5

        let simpleFowardMove: Move = {from: positionD4, to: positionD5, isValid: true}
        expect(isPossible.whitePawnMove(chessboard, simpleFowardMove)).toBeFalsy();
    });

    it("Pawns can move forward", () => {
        putPiece(chessboard, positionD4, pieces.whitePawn); // White Pawn at D4

        let simpleFowardMove: Move = {from: positionD4, to: positionD5, isValid: true}
        expect(isPossible.whitePawnMove(chessboard, simpleFowardMove)).toBeTruthy();
    });

    it("When in the initial position, pawns can move two squares forward", () => {
        putPiece(chessboard, positionD2, pieces.whitePawn); // White Pawn at D2
        let doubleFowardMove: Move = {from: positionD2, to: positionD4, isValid: true}
        expect(isPossible.whitePawnMove(chessboard, doubleFowardMove)).toBeTruthy();
    });

    it("When they have already moved, pawns cannot move two squares forward", () => {
        putPiece(chessboard, positionD3, pieces.whitePawn); // White Pawn at D3
        let doubleFowardMove: Move = {from: positionD3, to: positionD5, isValid: true}
        expect(isPossible.whitePawnMove(chessboard, doubleFowardMove)).toBeFalsy();
    });

    it("Pawns cannot capture an empty square", () => {
        putPiece(chessboard, positionD3, pieces.whitePawn); // White Pawn at D3
        let diagonalCapture: Move = {from: positionD3, to: positionC4, isValid: true}
        expect(isPossible.whitePawnMove(chessboard, diagonalCapture)).toBeFalsy();
    });

    it("Pawns cannot capture a piece of the same color", () => {
        putPiece(chessboard, positionD3, pieces.whitePawn); // White Pawn at D3
        putPiece(chessboard, positionC4, pieces.whitePawn); // White Pawn at C4

        let diagonalCapture: Move = {from: positionD3, to: positionC4, isValid: true}
        expect(isPossible.whitePawnMove(chessboard, diagonalCapture)).toBeFalsy();
    });

    it("Pawns can capture pieces of a different color", () => {
        putPiece(chessboard, positionD3, pieces.whitePawn); // White Pawn at D3
        putPiece(chessboard, positionC4, pieces.blackKing); // Black King at C4

        let diagonalCapture: Move = {from: positionD3, to: positionC4, isValid: true}
        expect(isPossible.whitePawnMove(chessboard, diagonalCapture)).toBeTruthy();
    })
});

/**
 * TODO: Unit tests for function kingMove()
 */
describe("Test kingMove()", () => {
    beforeEach( () => {
        chessboard = createEmptyChessboard();
    });

    it("A King can move 1 square in all directions", () => {
        let piecePosition : Position = positionE4;
        putPiece(chessboard, piecePosition, pieces.blackKing);

        let kingPosition : Array<Position> = [positionD3, positionD4, positionD5, positionE3, positionE5, positionF3, positionF4, positionF5];
        
        for (let i = 0 ; i < kingPosition.length ; i++) {
            expect(isPossible.kingMove(chessboard, {from: piecePosition, to: kingPosition[i], isValid: true})).toBeTruthy();
        } // Check it can move to squares D3, D4, D5, E3, E5, F3, F4, and F5
    })

    it("A King cannot move more than 1 square", () => {
		putPiece(chessboard, positionE4, pieces.blackKing);
		
		expect(isPossible.kingMove(chessboard, {from: positionE4, to: positionC2, isValid: true})).toBeFalsy();
		expect(isPossible.kingMove(chessboard, {from: positionE4, to: positionC3, isValid: true})).toBeFalsy();
		expect(isPossible.kingMove(chessboard, {from: positionE4, to: positionC4, isValid: true})).toBeFalsy();
		expect(isPossible.kingMove(chessboard, {from: positionE4, to: positionC6, isValid: true})).toBeFalsy();
		expect(isPossible.kingMove(chessboard, {from: positionE4, to: positionG4, isValid: true})).toBeFalsy();
		expect(isPossible.kingMove(chessboard, {from: positionE4, to: positionF6, isValid: true})).toBeFalsy();
		// Check it cannot move to squares C2, C3, C4, C6, H8, and G4
    })

    it("A King cannot capture pieces from the same color", () => {
        let piecePosition : Position = positionE5;
        putPiece(chessboard, piecePosition, pieces.blackKing);

        expect(isPossible.kingMove(chessboard, {from: piecePosition, to: piecePosition, isValid: true})).toBeFalsy();
        // Check the King cannot move to E5.
    })

    it("A King can capure pieces from a different color", () => {
        putPiece(chessboard, positionE5, pieces.whitePawn);
        putPiece(chessboard, positionE4, pieces.blackKing);
        
        expect(isPossible.kingMove(chessboard, {from: positionE4, to: positionE5, isValid: true})).toBeTruthy();
        // Place a white Pawn on E5
        // Check the King can move to E5.
    })
});

/**
 * TODO: Unit tests for function queenMove()
 */
describe("Test queenMove()", () => {
    beforeEach( () => {
        chessboard = createEmptyChessboard();
        putPiece(chessboard, positionE4, pieces.whiteQueen);
        // Initialize an empty chessboard
        // Place a white Queen on E4
    });

    it("A Queen can move diagonally", () => {
		expect(isPossible.queenMove(chessboard, moveE4_A8)).toBeTruthy();
		expect(isPossible.queenMove(chessboard, moveE4_B1)).toBeTruthy();
		expect(isPossible.queenMove(chessboard, moveE4_G6)).toBeTruthy();
		expect(isPossible.queenMove(chessboard, moveE4_H1)).toBeTruthy();
        // Check the following moves are possible: 
        // moveE4_A8, moveE4_B1, moveE4_G6, moveE4_H1
    });

    it("A Queen can move horizontally", () => {
        let moves : Array<Move> = [moveE4_H4, moveE4_A4];

        for (let i = 0 ; i < moves.length ; i++) {
            expect(isPossible.queenMove(chessboard, moves[i])).toBeTruthy();
        }
        // Check the following moves are possible: moveE4_H4, moveE4_A4
    });

    it("A Queen can move vertically", () => {
        let moves : Array<Move> = [moveE4_E1, moveE4_E8];

        for (let i = 0 ; i < moves.length ; i++) {
            expect(isPossible.queenMove(chessboard, moves[i])).toBeTruthy();
        }
        // Check the following moves are possible: moveE4_E1, moveE4_E8
    });

    it("A Queen can only move horizontally, vertically, and diagonally", () => {
        let moves : Array<Move> = [moveE4_C7, moveE4_B2];

        for (let i = 0 ; i < moves.length ; i++) {
            expect(isPossible.queenMove(chessboard, moves[i])).toBeFalsy();
        }
        // Check the following moves are impossible: moveE4_C7, moveE4_B2
    });

    it("A Queen cannot leap other pieces", () => {
        putPiece(chessboard, positionC6, pieces.whitePawn);
        putPiece(chessboard, positionF4, pieces.blackPawn);

        let moves : Array<Move> = [moveE4_A8, moveE4_H4];

        for (let i = 0 ; i < moves.length ; i++) {
            expect(isPossible.queenMove(chessboard, moves[i])).toBeFalsy();
        }      
        // Place a white Pawn on C6 and a black Pawn on F4
        // Check the moves moveE4_A8 and moveE4_H4 are impossible
    });

    it("A Queen cannot capure pieces from the same color", () => {
        putPiece(chessboard, positionH4, pieces.whitePawn);

        expect(isPossible.queenMove(chessboard, moveE4_H4)).toBeFalsy();
        // Place a white Pawn on H4
        // Check the move moveE4_H4 is impossible
    });

    it("A Queen can capure pieces from a different color", () => {
        putPiece(chessboard, positionH4, pieces.blackPawn);

        expect(isPossible.queenMove(chessboard, moveE4_H4)).toBeTruthy();
        // Place a black Pawn on H4
        // Check the move moveE4_H4 is possible
    });
});

/**
 * TODO: Unit tests for function bishopMove()
 */
describe("Test bishopMove()", () => {
    beforeEach( () => {
        chessboard = createEmptyChessboard();
        putPiece(chessboard, positionE4, pieces.blackBishop);
        // Initialize an empty chessboard
        // Place a black Bishop on E4
    });

    it("A Bishop can move diagonally", () => {
        expect(isPossible.bishopMove(chessboard, moveE4_A8)).toBeTruthy();
        expect(isPossible.bishopMove(chessboard, moveE4_B1)).toBeTruthy();
        expect(isPossible.bishopMove(chessboard, moveE4_G6)).toBeTruthy();
        expect(isPossible.bishopMove(chessboard, moveE4_H1)).toBeTruthy();   
        // Check the following moves are possible: 
        // moveE4_A8, moveE4_B1, moveE4_G6, moveE4_H1
    });

    it("A Bishop cannot move horizontally", () => {
        expect(isPossible.bishopMove(chessboard, moveE4_H4)).toBeFalsy();
        expect(isPossible.bishopMove(chessboard, moveE4_A4)).toBeFalsy();      
        // Check the following moves are impossible: moveE4_H4, moveE4_A4
    });

    it("A Bishop cannot move vertically", () => {
        expect(isPossible.bishopMove(chessboard, moveE4_E1)).toBeFalsy();
        expect(isPossible.bishopMove(chessboard, moveE4_E8)).toBeFalsy();
        // Check the following moves are impossible: moveE4_E1, moveE4_E8
    });

    it("A Bishop can capture a piece from another color", () => {
        putPiece(chessboard, positionA8, pieces.whitePawn);
        expect(isPossible.bishopMove(chessboard, moveE4_A8)).toBeTruthy();      
        // Place a white Pawn on A8
        // Check the move moveE4_A8 is possible

    });

    it("A Bishop cannot capture a piece from the same color", () => {
        putPiece(chessboard, positionA8, pieces.blackPawn);
        expect(isPossible.bishopMove(chessboard, moveE4_A8)).toBeFalsy();      
        // Place a black Pawn on A8
        // Check the move moveE4_A8 is impossible
    });

    it("A Bishop cannot leap other pieces", () => {
        putPiece(chessboard, positionC6, pieces.whitePawn);
        expect(isPossible.bishopMove(chessboard, moveE4_A8)).toBeFalsy();      
        // Place a white Pawn on C6
        // Check the move moveE4_A8 is impossible
   });
});

/**
 * TODO: Unit tests for function knightMove()
 */
describe("Test knightMove()", () => {
    beforeEach( () => {
        chessboard = createEmptyChessboard();
        putPiece(chessboard, positionE4, pieces.blackKnight);
        // Initialize an empty chessboard
    });
    it("A Knight can move two squares horizontally and one square vertically", () => {
        expect(isPossible.knightMove(chessboard, moveE4_D6)).toBeTruthy();
        expect(isPossible.knightMove(chessboard, moveE4_F6)).toBeTruthy();
        expect(isPossible.knightMove(chessboard, moveE4_D2)).toBeTruthy();
        expect(isPossible.knightMove(chessboard, moveE4_F2)).toBeTruthy();
        // Check the following moves are possible: moveE4_E1, moveE4_F6, moveE4_D2, moveE4_F2
    })
    it("A Knight can move two squares vertically and one square horizontally", () => {
        expect(isPossible.knightMove(chessboard, moveE4_C5)).toBeTruthy();
        expect(isPossible.knightMove(chessboard, moveE4_G5)).toBeTruthy();
        expect(isPossible.knightMove(chessboard, moveE4_C3)).toBeTruthy();
        expect(isPossible.knightMove(chessboard, moveE4_G3)).toBeTruthy();
        // Check the following moves are possible: moveE4_C5, moveE4_G5, moveE4_C3, moveE4_G3
    })
    it("A Knight can 'jump' obstacles" , () => {
        putPiece(chessboard, positionF3, pieces.whitePawn);
        putPiece(chessboard, positionF4, pieces.blackPawn);

        expect(isPossible.knightMove(chessboard, moveE4_G3)).toBeTruthy();
    })
    it("A Knight cannot move diagonally", () => {
        expect(isPossible.knightMove(chessboard, moveE4_G6)).toBeFalsy();
    });
    it("A Knight cannot move horizontally", () => {
        expect(isPossible.knightMove(chessboard, moveE4_E6)).toBeFalsy();
    });
    it("A Knight cannot move vertically", () => {
        expect(isPossible.knightMove(chessboard, moveE4_G4)).toBeFalsy();
    });
    it("A Knight can capture a piece from another color", () => {
        putPiece(chessboard, positionG3, pieces.whitePawn);

        expect(isPossible.knightMove(chessboard, moveE4_G3)).toBeTruthy();
    });
    it("A Knight cannot capture a piece from the same color", () => {
        putPiece(chessboard, positionG3, pieces.blackPawn);

        expect(isPossible.knightMove(chessboard, moveE4_G3)).toBeFalsy();
    });
});

/**
 * TODO: Unit tests for function rookMove()
 */
describe("Test rookMove()", () => {
    beforeEach( () => {
        chessboard = createEmptyChessboard();
        putPiece(chessboard, positionE4, pieces.whiteRook);
        // Initialize an empty chessboard
        // Place a white Rook on E4
    });

    it("A roock can move horizontally", () => {
        expect(isPossible.rookMove(chessboard, moveE4_H4)).toBeTruthy();
        expect(isPossible.rookMove(chessboard, moveE4_A4)).toBeTruthy();
        // Check the following moves are possible: moveE4_H4, moveE4_A4
    });

    it("A roock can move vertically", () => {
        expect(isPossible.rookMove(chessboard, moveE4_E1)).toBeTruthy();
        expect(isPossible.rookMove(chessboard, moveE4_E8)).toBeTruthy();        
        // Check the following moves are possible: moveE4_E1, moveE4_E8
    });

    it("A roock cannot move diagonally", () => {
		expect(isPossible.rookMove(chessboard, moveE4_H7)).toBeFalsy();
		expect(isPossible.rookMove(chessboard, moveE4_H1)).toBeFalsy();
		expect(isPossible.rookMove(chessboard, moveE4_A8)).toBeFalsy();
		expect(isPossible.rookMove(chessboard, moveE4_B1)).toBeFalsy();	
        // Check the following moves are impossible: 
        // moveE4_A8, moveE4_B1, moveE4_H7, moveE4_H1

    });

    it("A roock can capture a piece from another color", () => {
		putPiece(chessboard, positionH4, pieces.blackPawn);
		
		expect(isPossible.rookMove(chessboard, moveE4_H4)).toBeTruthy();
        // Place a black Pawn on H4
        // Check the move moveE4_H4 is possible
    });

    it("A roock cannot capture a piece from the same color", () => {
		putPiece(chessboard, positionH4, pieces.whitePawn);
		
		expect(isPossible.rookMove(chessboard, moveE4_H4)).toBeFalsy();
        // Place a white Pawn on H4
        // Check the move moveE4_H4 is impossible
    });

    it("A Roock cannot leap other pieces", () => {
		putPiece(chessboard, positionF4, pieces.blackPawn);
		
		expect(isPossible.rookMove(chessboard, moveE4_H4)).toBeFalsy();	
        // Place a black Pawn on F4
        // Check the move moveE4_H4 is impossible
   });
});

describe("Test isPromotionableve()", () => {
	beforeEach( () => {
		chessboard = createEmptyChessboard();
		// Initialize an empty chessboard
	});

	it("Black Pawn is promotionable", () => {
		putPiece(chessboard, positionE2, pieces.blackPawn);

		expect(isPossible.isPromotionable(moveE2_E1)).toBeTruthy();
	});

	it("White Pawn is promotionable", () => {
		putPiece(chessboard, positionE7, pieces.whitePawn);

		expect(isPossible.isPromotionable(moveE7_E8)).toBeTruthy();
	});

	it("Other pieces (bishop) aren't promotionable", () => {
		putPiece(chessboard, positionB5, pieces.whiteBishop);

		expect(isPossible.isPromotionable(moveB5_E8)).toBeFalsy();
	});
});