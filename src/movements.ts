import { Chessboard, squareAtPosition, pieceAtPosition, Square, isEmpty} from './chessboard'
import { Position, position, equals } from "./position";
import * as pieces from './piece'
import { Piece }  from './piece'
import * as isPossible from './move-validation'

const VALID_MOVE_STRING: RegExp = new RegExp('([a-z]|[A-Z])([1-8])-([A-H]|[a-z])([1-8])')

export interface Move {
    isValid : boolean;
    from?   : Position;
    to?     : Position;
	piece?  : Piece; // Rajouté piece
	promotion? : Piece; // Rajouté promotion
}

/**
 * Creates a new Move from two Positions, representing
 * the Move's initial and final position.
 * 
 * @param board The chessboard for the current game
 * @param from The initial position
 * @param to The final position
 * @returns the move in a Move interface
 */
export function move(board: Chessboard, from: Position, to: Position): Move {
    let move: Move = {isValid: true, from: from, to: to, piece: pieceAtPosition(board, from)}; // Rajouté piece
    return move;
}

/**
 * Processes a move received from a client browser.
 * If the move is valid and possible, the move is performed and this function
 * returns true. Otherwise, it returns false
 * 
 * @param board The chessboard for the current game
 * @param moveString The string received from the client containing a move
 * @returns true, if the move is valid and possible
 */
export function processMove(board: Chessboard, moveString: string): boolean {
	let currentMove : Move = parseMoveString(board, moveString);
	
    if (currentMove.isValid && !isEmpty(board, currentMove.from!) && isYourTurn(board, currentMove.piece!) && isMovePossible(board, currentMove)) {
        
        // Clone l'objet
        let boardTest = JSON.parse(JSON.stringify(board));

        performMove(boardTest, currentMove);

        if (isPositionControlled(boardTest, yourKingPosition(boardTest))) {
            console.log("Invalid movement! (Roi en échec)");
            
            return false;
        }
        else {
            performMove(board, currentMove);

            // Ajout nouveau tour
            console.log(`Nombre de coups : ${board.turn}`);
            board.turn++;
    
            // Enregistrement history
            board.history.push(currentMove);
            console.log(board.history);
    
            return true;
        }
    } else {
        console.log("Invalid movement!");
        return false;
    }

}

/**
 * Verify if the piece you're trying to move is the right color for this turn
 * 
 * @param board The chessboard for the current game
 * @param piece The piece you're trying to move
 * @returns true, if it's the right piece color
 */
export function isYourTurn(board: Chessboard, piece: Piece): boolean {
    return piece!.isWhite === (board.turn%2 === 1);
}

/**
 * Verify if a position can be attacked
 * 
 * @param board The chessboard for the current game
 * @param pos The place you're verifying
 * @returns true, if it's controlled
 */
export function isPositionControlled(board: Chessboard, pos: Position): boolean {
    for( let x: number = 0; x <= 7; x++) {
        for( let y: number = 0; y <= 7; y++) {
            if(!isEmpty(board, position(x,y)) && !isYourTurn(board, pieceAtPosition(board, position(x, y))) && isMovePossible(board, move(board, position(x, y), pos))){
                return true;
			}
        }
	}
    return false;
}

/**
 * Find your king position
 * 
 * @param board The chessboard for the current game
 * @returns position of your king, in a Position interface
 * 
 */
export function yourKingPosition(board: Chessboard): Position {
    for( let x: number = 0; x <= 7; x++) {
        for( let y: number = 0; y <= 7; y++) {
            if(!isEmpty(board, position(x,y)) && pieceAtPosition(board, position(x, y)).type === "King" && isYourTurn(board, pieceAtPosition(board, position(x, y)))){
                return position(x, y);
			}
        }
	}
    // Impossible
	return position(0, 0);
} 

/**
 * Pour tester plus simplement l'Echec et le Pat: TEMPORAIRE
 * 
 * @param board The chessboard for the current game
 * 
 */
export function killPawns(board: Chessboard) {
    for( let x: number = 0; x <= 7; x++) {
        for( let y: number = 0; y <= 7; y++) {
            if(!isEmpty(board, position(x,y)) && pieceAtPosition(board, position(x, y)).type === "Pawn"){
                squareAtPosition(board, position(x, y)).isEmpty = true;
			}
        }
	}
}

/**
 * Verify if a piece moved
 * 
 * @param board The chessboard of the current game
 * @param currentPos The position of the piece you're trying to check
 * @returns true, if it moved
 */
export function didMove(board: Chessboard, currentPos: Position): boolean {
    for(var i = 0; i < board.history.length; i++) {
        if(equals(board.history[i].from!, currentPos)) {
            return true;
        }
    }
    return false;
}

/**
 * Detect if you can perform a move
 * 
 * @param board The chessboard for the current game
 * @returns True, if your can move at least a piece
 * 
 */
export function canMove(board: Chessboard): boolean {
    // Clone l'objet
    for( let x1: number = 0; x1 <= 7; x1++) {
        for( let y1: number = 0; y1 <= 7; y1++) {
            if(!isEmpty(board, position(x1,y1)) && isYourTurn(board, pieceAtPosition(board, position(x1, y1)))){
                for( let x2: number = 0; x2 <= 7; x2++) {
                    for( let y2: number = 0; y2 <= 7; y2++) {
                        let boardTest = JSON.parse(JSON.stringify(board));
                        performMove(boardTest, move(board, position(x1, y1), position(x2, y2)));
                        if(isMovePossible(board, move(board, position(x1, y1), position(x2, y2))) && !isPositionControlled(boardTest, yourKingPosition(boardTest))) {
                            console.log(JSON.stringify(position(x1, y1)) + " vers " + JSON.stringify(position(x2, y2)) + " possible");
                            return true;
                        }
                    }
                }
			}
        }
    }
    return false;
}

/**
 * Parses a string in the format "A1-F8" and returns a Move.
 * If the format is not valid, returns a Move with isValid === false.
 * 
 * @param board The chessboard for the current game
 * @param movementString A 5 characters string containing a move
 * @returns the move in a Move interface
 */
export function parseMoveString(board: Chessboard, movementString: string): Move {
	let newMove : Move;
	
    if (movementString.length != 5 ||  ! movementString.match(VALID_MOVE_STRING)) {
        newMove = { isValid : false };
    } else {
        let fromFile  : number = movementString.charCodeAt(0);
        let fromRank  : number = parseInt(movementString[1]);
        let toFile    : number = movementString.charCodeAt(3);
        let toRank    : number = parseInt(movementString[4]);

        // In Unicode, charCode('A') == 65, charCode('a') == 97
        // Remember that Arrays start from [0][0] == position 'A1'
        let from : Position = {rank : fromRank -1, file: fromFile > 90 ? fromFile - 97 : fromFile - 65}
        let to   : Position = {rank : toRank -1, file: toFile > 90 ? toFile - 97 : toFile - 65 }

        newMove = move(board, from, to);
	}

    return newMove;
}

/**
 * Checks whether a move is possible in the given chessboard
 * 
 * @param board The chessboard for the current game
 * @param currentMove The Move you want to check
 * @returns true, if the move is possible
 */

export function isMovePossible(board: Chessboard, currentMove: Move): boolean {
    let square: Square = squareAtPosition(board, currentMove.from!);
    if (square.isEmpty) { return false;}

    let piece: Piece = square.piece!;

    switch(piece.name) {
        case pieces.whitePawn.name  : return isPossible.whitePawnMove(board, currentMove);
        case pieces.blackPawn.name  : return isPossible.blackPawnMove(board, currentMove);
        case pieces.whiteKing.name  : return isPossible.kingMove(board, currentMove);
        case pieces.whiteQueen.name : return isPossible.queenMove(board, currentMove);
        case pieces.whiteBishop.name: return isPossible.bishopMove(board, currentMove);
        case pieces.whiteKnight.name: return isPossible.knightMove(board, currentMove);
        case pieces.whiteRook.name  : return isPossible.rookMove(board, currentMove);
        case pieces.blackKing.name  : return isPossible.kingMove(board, currentMove);
        case pieces.blackQueen.name : return isPossible.queenMove(board, currentMove);
        case pieces.blackBishop.name: return isPossible.bishopMove(board, currentMove);
        case pieces.blackKnight.name: return isPossible.knightMove(board, currentMove);
        case pieces.blackRook.name  : return isPossible.rookMove(board, currentMove);
    }
    console.log("erreur, pièce non trouvée: " + JSON.stringify(piece));
    return false;
}

/**
 * Execute the move
 * @param board The chessboard for the current game
 * @param currentMove The Move you want to do
 */
export function performMove(board : Chessboard, currentMove : Move) {
    let source      : Square = squareAtPosition(board, currentMove.from!);
    let destination : Square = squareAtPosition(board, currentMove.to!);

    destination.piece = source.piece;
    destination.isEmpty = false;
    source.isEmpty = true;
}
