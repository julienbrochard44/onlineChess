import * as pieces from "./piece";
import { Piece } from "./piece";
import { move, Move, performMove } from "./movements";
import { position, Position } from "./position";

export function isEmpty(chessboard : Chessboard, position : Position): boolean {
    let square: Square = squareAtPosition(chessboard, position);
    return square.isEmpty;
}

export interface Square {
    position : Position
    isEmpty  : boolean
    piece?    : Piece
  }

export interface Chessboard {
    board : Array<Array<Square>>
    turn : number
    history : Array<Move>
}

export function squareAtPosition(chessboard: Chessboard, position : Position): Square {
    let square: Square = chessboard.board[position.file][position.rank];

    return square;
}

export function pieceAtPosition(chessboard: Chessboard, position : Position): Piece {
    let square: Square = squareAtPosition(chessboard, position);
    return square.piece!;
}

// Retourne un échiquier initialisé en début de partie 
export function createInitialChessboard(): Chessboard {
    let chessboard : Chessboard = createChessboard();

	// ranks 2 - 6 are empty
    for(let rank: number = 2; rank < 6; rank++) {
        for(let col: number = 0; col < 8; col++) {
            let position: Position = {rank : rank, file : col};
            let square : Square = {position : position, isEmpty : true};
            chessboard.board[col][rank] = square;
        }
    }

    // Pawns in ranks 2 and 6
    let position: Position;
    let square : Square;
    for(let col: number = 0; col < 8; col++) {
        putPieceAtCoordinate(chessboard, col, 1, pieces.whitePawn);
        putPieceAtCoordinate(chessboard, col, 6, pieces.blackPawn);
    }

    // Kings and Queens
    putPieceAtCoordinate(chessboard, 4, 0, pieces.whiteKing);
    putPieceAtCoordinate(chessboard, 4, 7, pieces.blackKing);
    putPieceAtCoordinate(chessboard, 3, 0, pieces.whiteQueen);
    putPieceAtCoordinate(chessboard, 3, 7, pieces.blackQueen);

    // Bishops
    putPieceAtCoordinate(chessboard, 2, 0, pieces.whiteBishop);
    putPieceAtCoordinate(chessboard, 2, 7, pieces.blackBishop);
    putPieceAtCoordinate(chessboard, 5, 0, pieces.whiteBishop);
    putPieceAtCoordinate(chessboard, 5, 7, pieces.blackBishop);

    // Knights
    putPieceAtCoordinate(chessboard, 1, 0, pieces.whiteKnight);
    putPieceAtCoordinate(chessboard, 1, 7, pieces.blackKnight);
    putPieceAtCoordinate(chessboard, 6, 0, pieces.whiteKnight);
    putPieceAtCoordinate(chessboard, 6, 7, pieces.blackKnight);

    // Rooks
    putPieceAtCoordinate(chessboard, 0, 0, pieces.whiteRook);
    putPieceAtCoordinate(chessboard, 0, 7, pieces.blackRook);
    putPieceAtCoordinate(chessboard, 7, 0, pieces.whiteRook);
    putPieceAtCoordinate(chessboard, 7, 7, pieces.blackRook);

    return chessboard;
}

export function createEmptyChessboard(): Chessboard {
    let newChessboard : Chessboard = createChessboard();

    for(let rank: number = 0; rank < 8; rank++) {
        for(let col: number = 0; col < 8; col++) {
            let position: Position = {rank : rank, file : col};
            let square : Square = {position : position, isEmpty : true};
            newChessboard.board[col][rank] = square;
        }
    }

    return newChessboard;
}

function createChessboard(): Chessboard {
    let board : Square[][] = []
    for (let i = 0 ; i < 8 ; i++) {
        board[i] = [];
    }

    let newChessboard : Chessboard = {
        turn : 1,
        board : board,
        history : []
	};
	
    return newChessboard;

}

function putPieceAtCoordinate(chessboard: Chessboard, file : number, rank: number, piece : Piece) {
    let position : Position = {rank : rank, file : file};
    return putPiece(chessboard, position, piece);
}

export function putPiece(chessboard: Chessboard, position: Position, piece : Piece) {
    let board : Array<Array<Square>> = chessboard.board;
    let square : Square = { position : position, isEmpty : false, piece : piece };
    board[position.file][position.rank] = square;
}

export function reset(board : Chessboard):Chessboard {
    return createInitialChessboard();
}

export function back(board : Chessboard):Chessboard {
	let turn : number = board.turn;
	let history : Array<Move> = board.history;
    board = createInitialChessboard();
    
    for (let i : number = 0 ; i < turn - 2 ; i++) {
		performMove(board, history[i]);
        
        // Castling
        if(history[i].piece!.type == "King" && Math.abs(history[i].from!.file - history[i].to!.file) == 2){
            if(history[i].to!.file - history[i].from!.file < 0){
                performMove(board, move(board, position(0, history[i].from!.rank),position(history[i].to!.file+1, history[i].from!.rank)));
            }
            else {
                performMove(board, move(board, position(7, history[i].from!.rank),position(history[i].to!.file-1, history[i].from!.rank)));
            }
        }

        // Promotion
		if (history[i].promotion !== undefined) {
			putPiece(board, history[i].to!, history[i].promotion!);
		}

		board.history.push(history[i]);
		board.turn++;
	}
	
	console.log("Back");
 
    return board;
}