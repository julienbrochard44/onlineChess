import { Chessboard, isEmpty, Square, squareAtPosition, putPiece, pieceAtPosition } from "./chessboard";
import { Move, move, performMove, isPositionControlled, didMove } from "./movements";

import { equals, left, right, bottom, top, position } from "./position";
import { whiteQueen, blackQueen, whiteBishop, blackBishop, whiteRook, blackRook, whiteKnight, blackKnight, Piece } from "./piece";
import { isString } from "util";

/**
 * Verify if a pawn is in a Promotionable position
 * 
 * @param board The chessboard of the current game
 * @param currentMove The Move you want to check
 * @returns True if the move is Promotionable
 */
export function isPromotionable(currentMove: Move): boolean {
        if (currentMove.piece!.name === "White Pawn") {
            // White pawn
            if (currentMove.to!.rank === 7) {
                return true;
            }
        } else if (currentMove.piece!.name === "Black Pawn") {
            // Black pawn
            if (currentMove.to!.rank === 0) {
                return true;
            }
		}
		
	return false;
}

/**
 * Put the promotion piece the player has selected
 * 
 * @param board The chessboard of the current game
 * @param currentMove The Move you want to check
 */
export function promotionChoice(board: Chessboard, promotionPiece: string, currentMove: Move) {
	const queenPiece : Piece = currentMove.to!.rank === 7 ? whiteQueen : blackQueen;
	const knightPiece : Piece = currentMove.to!.rank === 7 ? whiteKnight : blackKnight;
	const bishopPiece : Piece = currentMove.to!.rank === 7 ? whiteBishop : blackBishop;
	const rookPiece : Piece = currentMove.to!.rank === 7 ? whiteRook : blackRook;

	switch (promotionPiece) {
		case "Queen" :
			putPiece(board, currentMove.to!, queenPiece);
			board.history[board.history.length - 1].promotion = pieceAtPosition(board, currentMove.to!); // Put in history
		break;
		case "Knight" :
			putPiece(board, currentMove.to!, knightPiece);
			board.history[board.history.length - 1].promotion = pieceAtPosition(board, currentMove.to!); // Put in history
		break;
		case "Bishop" :
			putPiece(board, currentMove.to!, bishopPiece);
			board.history[board.history.length - 1].promotion = pieceAtPosition(board, currentMove.to!); // Put in history
		break;
		case "Rook" :
			putPiece(board, currentMove.to!, rookPiece);
			board.history[board.history.length - 1].promotion = pieceAtPosition(board, currentMove.to!); // Put in history
		break;
	}
}

/**
 * PION NOIR
 * 
 * @param board The chessboard of the current game
 * @param currentMove The Move you want to check
 * @returns True if the move has been executed
 */
export function blackPawnMove(board: Chessboard, currentMove: Move): boolean {
    // En passant
    if(board.history.length > 0){
        let lastMove : Move = board.history[board.history.length-1];

        if (lastMove.piece!.name === "White Pawn" && lastMove.from!.rank === 1 && lastMove.to!.rank === 3 && equals(currentMove.to!, top(lastMove.to!))) {
            squareAtPosition(board, lastMove.to!).isEmpty = true;
            return isEmpty(board, currentMove.to!);
        }
    }

    if (equals(currentMove.to!, top(currentMove.from!))) {
        return isEmpty(board, currentMove.to!);
    }

    if (currentMove.from!.rank === 6 && equals(currentMove.to!, top(top(currentMove.from!)))) {
        return isEmpty(board, top(currentMove.from!)) && isEmpty(board, currentMove.to!);
    }

    if (equals(currentMove.to!, left(top(currentMove.from!))) || equals(currentMove.to!, right(top(currentMove.from!)))) {
		let destination: Square = squareAtPosition(board, currentMove.to!);
		
        return !(destination.isEmpty || !destination.piece!.isWhite);
	}
	
    return false;
}

/**
 * PION BLANC
 * 
 * @param board The chessboard of the current game
 * @param currentMove The Move you want to check
 * @returns True if the move has been executed
 */
export function whitePawnMove(board: Chessboard, currentMove: Move): boolean {
    // En passant
    if(board.history.length > 0){
        let lastMove : Move = board.history[board.history.length-1];

        if (board.turn > 1) {
            if (lastMove.piece!.name === "Black Pawn" && lastMove.from!.rank === 6 && lastMove.to!.rank === 4 && equals(currentMove.to!, bottom(lastMove.to!))) {
                squareAtPosition(board, lastMove.to!).isEmpty = true;
                return isEmpty(board, currentMove.to!);
            }
        }
    }
	
    if (equals(currentMove.to!, bottom(currentMove.from!))) {
        return isEmpty(board, currentMove.to!);
    }

    if (currentMove.from!.rank === 1 && equals(currentMove.to!, bottom(bottom(currentMove.from!)))) {
        return isEmpty(board, bottom(currentMove.from!)) && isEmpty(board, currentMove.to!);
    }

    if (equals(currentMove.to!, left(bottom(currentMove.from!))) || equals(currentMove.to!, right(bottom(currentMove.from!)))) {
		let destination: Square = squareAtPosition(board, currentMove.to!);
		
        return !(destination.isEmpty || destination.piece!.isWhite);
	}

    return false;
}

/**
 * ROI
 * 
 * @param board The chessboard of the current game
 * @param currentMove The Move you want to check
 * @returns True if the move has been executed
 */
export function kingMove(board: Chessboard, currentMove: Move): boolean {

    if (isKingValid(board, currentMove)) {
        return isDestinationValid(board, currentMove);
    } 
    else {
        return performCastling(board, currentMove);
        
    }
}

/**
 * VALIDATION ROI
 * 
 * @param board The chessboard of the current game
 * @param currentMove The Move you want to check
 * @returns True if the move is valid
 */
function isKingValid(board: Chessboard, currentMove: Move): boolean {
    return(
        ((Math.abs(currentMove.from!.rank - currentMove.to!.rank) === 0) || (Math.abs(currentMove.from!.rank - currentMove.to!.rank) === 1)) 
        &&
        ((Math.abs(currentMove.from!.file - currentMove.to!.file) === 0) || (Math.abs(currentMove.from!.file - currentMove.to!.file) === 1))
    );
}

/**
 * VALIDATION ROQUE
 * 
 * @param board The chessboard of the current game
 * @param currentMove The Move you want to check
 * @returns True if the castling has been executed
 */
function performCastling(board: Chessboard, currentMove: Move): boolean {
    let source: Square = squareAtPosition(board, currentMove.from!);
    // Si le mouvement est celui d'un Roque
    if(Math.abs(currentMove.from!.file - currentMove.to!.file) == 2 && currentMove.from!.rank == currentMove.to!.rank && !didMove(board, source.position)) {
        // Si le Roque est vers la gauche
        if(currentMove.to!.file - currentMove.from!.file < 0){
            if(!didMove(board, position(0, currentMove.from!.rank))){
                if(
                isStraightValid(board, move(board, currentMove.from!, position(0, currentMove.from!.rank))) 
                && !isStraightControlled(board, move(board, currentMove.from!, currentMove.to!))
                && !isPositionControlled(board, currentMove.to!)
                ) {
                    performMove(board, move(board, position(0, currentMove.from!.rank),position(currentMove.to!.file+1, currentMove.from!.rank)));
                    return true;
                }
            }
        }
        // Si le Roque ets vers la droite
        else {
            if(!didMove(board, position(7, currentMove.from!.rank))){
                if(
                isStraightValid(board, move(board, currentMove.from!, position(7, currentMove.from!.rank)))
                && !isStraightControlled(board, move(board, currentMove.from!, currentMove.to!))
                && !isPositionControlled(board, currentMove.to!)
                ) {
                    performMove(board, move(board, position(7, currentMove.from!.rank),position(currentMove.to!.file-1, currentMove.from!.rank)));
                    return true;
                }
            }  
        }
    }
    return false;
}

/**
 * REINE
 * 
 * @param board The chessboard of the current game
 * @param currentMove The Move you want to check
 * @returns True if the move has been executed
 */
export function queenMove(board: Chessboard, currentMove: Move): boolean {
    if (isStraightValid(board, currentMove) || isDiagonalValid(board, currentMove)) {
        return isDestinationValid(board, currentMove);
    }
    return false;
}

/**
 * TOUR
 * 
 * @param board The chessboard of the current game
 * @param currentMove The Move you want to check
 * @returns True if the move has been executed
 */
export function rookMove(board: Chessboard, currentMove: Move): boolean {
    if (currentMove.from!.rank === currentMove.to!.rank || currentMove.from!.file === currentMove.to!.file) {
        if (isStraightValid(board, currentMove)) {
            return isDestinationValid(board, currentMove);
        }
    }
    return false;
}

/**
 * FOU
 * 
 * @param board The chessboard of the current game
 * @param currentMove The Move you want to check
 * @returns True if the move has been executed
 */
export function bishopMove(board: Chessboard, currentMove: Move): boolean {
    // #TODO: Implement this function
    if (Math.abs(currentMove.from!.rank - currentMove.to!.rank) === Math.abs(currentMove.from!.file - currentMove.to!.file)) {
        if (isDiagonalValid(board, currentMove)) {
            return isDestinationValid(board, currentMove); 
        }
    }
    return false;
}

/**
 * CAVALIER
 * 
 * @param board The chessboard of the current game
 * @param currentMove The Move you want to check
 * @returns True if the move has been executed
 */
export function knightMove(board: Chessboard, currentMove: Move): boolean {
    // #TODO: Implement this function
    if (isKnightValid(board, currentMove)) {
       return isDestinationValid(board, currentMove); 
    }
    return false;
}

/**
 * VALIDATION CAVALIER
 * 
 * @param board The chessboard of the current game
 * @param currentMove The Move you want to check
 * @returns True if the move is valid
 */
function isKnightValid(board: Chessboard, currentMove: Move): boolean {
    return(
        (Math.abs(currentMove.from!.rank - currentMove.to!.rank) === 2 && Math.abs(currentMove.from!.file - currentMove.to!.file) === 1)
        ||
        (Math.abs(currentMove.from!.rank - currentMove.to!.rank) === 1 && Math.abs(currentMove.from!.file - currentMove.to!.file) === 2)
    );
}



/**
 * VALIDATION MOUVEMENTS LIGNES/COLONNES
 * 
 * @param board The chessboard of the current game
 * @param currentMove The Move you want to check
 * @returns True if the move is valid
 */
export function isStraightValid(board: Chessboard, currentMove: Move): boolean {
    // Straight moves
    if(currentMove.from!.rank == currentMove.to!.rank || currentMove.from!.file == currentMove.to!.file){
        for (let i = 1; i < Math.abs(currentMove.from!.rank - currentMove.to!.rank)+Math.abs(currentMove.from!.file - currentMove.to!.file); i++) {
            if ((!isEmpty(board, position(
                currentMove.from!.file + i * Math.sign(currentMove.to!.file - currentMove.from!.file)/* 0: Colones, sinon Lignes  */
                , 
                currentMove.from!.rank + i * Math.sign(currentMove.to!.rank - currentMove.from!.rank)))/* -1: Monte ou Gauche ; +1 Descend ou Droite */
                )
            ){
                //console.log("Straight not valid");
                return false;
            }
        }
        return true;
    }
    return false;
}

/**
 * VALIDATION MOUVEMENTS LIGNES/COLONNES
 * 
 * @param board The chessboard of the current game
 * @param currentMove The Move you want to check
 * @returns True if the straight move is controlled
 */
export function isStraightControlled(board: Chessboard, currentMove: Move): boolean {
    // Straight moves
    if(currentMove.from!.rank == currentMove.to!.rank || currentMove.from!.file == currentMove.to!.file){
        for (let i = 1; i < Math.abs(currentMove.from!.rank - currentMove.to!.rank)+Math.abs(currentMove.from!.file - currentMove.to!.file); i++) {
            if ((isPositionControlled(board, position(
                currentMove.from!.file + i * Math.sign(currentMove.to!.file - currentMove.from!.file)/* 0: Colones, sinon Lignes  */
                , 
                currentMove.from!.rank + i * Math.sign(currentMove.to!.rank - currentMove.from!.rank)))// -1: Monte ou Gauche ; +1 Descend ou Droite 
                )
            ){
                //console.log("Straight controlled");
                return true;
            }
        }
        return false;
    }
    return true;
}

/**
 * VALIDATION MOUVEMENT DIAGONAL
 * 
 * @param board The chessboard of the current game
 * @param currentMove The Move you want to check
 * @returns True if the move is valid
 */
export function isDiagonalValid(board: Chessboard, currentMove: Move): boolean {
    // Diagonal moves
    if(Math.abs(currentMove.from!.rank - currentMove.to!.rank) == Math.abs(currentMove.from!.file - currentMove.to!.file)){
        for (let i : number = 1 ; i < Math.abs(currentMove.from!.rank - currentMove.to!.rank) ; i++) {
            if (!isEmpty(board, position(
                currentMove.from!.file + i * Math.sign(currentMove.to!.file - currentMove.from!.file) /* -1: Gauche ; +1: Droite */
                , 
                currentMove.from!.rank + i * Math.sign(currentMove.to!.rank - currentMove.from!.rank) /* -1: Descend ; +1: Monte*/
            ))){
                return false;
            }
        }
        return true; 
    }
    return false;
}

/**
 * VALIDATION DESTINATION
 * 
 * @param board The chessboard of the current game
 * @param currentMove The Move you want to check
 * @returns True if the destination is valid
 */
export function isDestinationValid(board: Chessboard, currentMove: Move): boolean {
    let source: Square = squareAtPosition(board, currentMove.from!);
    let destination: Square = squareAtPosition(board, currentMove.to!);
    return (destination.isEmpty || (destination.piece!.isWhite !== source.piece!.isWhite));
}

