import express = require("express");
import bodyParser = require("body-parser");

const PORT: number = 8080;
const PUBLIC_DIR = "client";
const app = express();

import { Chessboard, createInitialChessboard, reset, back, isEmpty } from "./chessboard";
import { processMove, parseMoveString, Move, isPositionControlled, yourKingPosition, isMovePossible, isYourTurn, canMove, killPawns} from "./movements";
import { isPromotionable, promotionChoice } from "./move-validation";

class HttpServer {
	port : number;

	constructor(port: number) {
		this.port = port;
	}

	public onStart(): void {
		let board : Chessboard = createInitialChessboard();
		let app : express.Application = express();

		app.use(bodyParser.json());
		app.use(bodyParser.urlencoded({ extended: true }));
		app.use(express.static(PUBLIC_DIR));
		app.set("view engine", "ejs");

		app.listen(this.port, () => {
			console.log(`Application lancée à l'adresse http://localhost:${this.port}`);
		});

		app.get('/', (req: express.Request, res: express.Response) => {
			res.render('index', { error : "", promotionChoice : "", turn : "WHITE" });
		})

		app.get("/status.js", (req: express.Request, res: express.Response) => {
			res.end("var boardJSON= " + JSON.stringify(board));
		});

		app.post("/", (req: express.Request, res: express.Response) => {
			// Move
			let unparsedMove : string = req.body.move;
			let currentMove : Move = parseMoveString(board, unparsedMove);

			let didPerfom : boolean = processMove(board, unparsedMove);
			let message : string = didPerfom ? "" : "Invalid movement!";

			let colorTurn : string = (board.history.length > 0 && board.history[board.history.length-1].piece!.isWhite) ? "BLACK" : "WHITE";

			// Promotion
			let promoMsg : string = "";
			if (didPerfom && isPromotionable(currentMove)) {
				promoMsg = "Promotion ! Quelle pièce souhaitez-vous ?";
			}

			// test Pat et Echec
			if (!canMove(board)) {
				if (isPositionControlled(board, yourKingPosition(board))){
					message = "Checkmate!"
				} else {
					message = "Stalemate!"
				}
			}
	
			res.render("index", { error : message, promotionChoice : promoMsg, turn : colorTurn });
		});

		app.post("/reset", (req : express.Request, res : express.Response) => {
			board = reset(board);
			res.redirect("/");
		});

		app.post("/back", (req : express.Request, res : express.Response) => {
			board = back(board);
			res.redirect("/");
		});

		app.post("/promotion", (req: express.Request, res: express.Response) => {
			let promotionPiece : string = req.body.promotionPiece;
			let lastMove : Move = board.history[board.history.length-1];

			promotionChoice(board, promotionPiece, lastMove);
			res.redirect("/");
		});
	}
}

let server: HttpServer = new HttpServer(PORT)
server.onStart();