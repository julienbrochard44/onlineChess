
export interface Piece {
    symbol    : string,  
    isWhite   : boolean,
    name      : string,
    type      : string
}

export const whitePawn     : Piece = {symbol : "♙", name: "White Pawn", type: "Pawn", isWhite : true};
export const whiteKing     : Piece = {symbol : "♔", name: "White King", type: "King", isWhite : true};
export const whiteQueen    : Piece = {symbol : "♕", name: "White Queen", type: "Queen", isWhite : true};
export const whiteRook     : Piece = {symbol : "♖", name: "White Rook", type: "Rook", isWhite : true};
export const whiteKnight   : Piece = {symbol : "♘", name: "White Knight", type: "Knight", isWhite : true};
export const whiteBishop   : Piece = {symbol : "♗", name: "White Bishop", type: "Bishop", isWhite : true};

export const blackPawn     : Piece = {symbol : "♟", name: "Black Pawn", type: "Pawn", isWhite : false};
export const blackKing     : Piece = {symbol : "♚", name: "Black King", type: "King", isWhite : false};
export const blackQueen    : Piece = {symbol : "♛", name: "Black Queen", type: "Queen", isWhite : false};
export const blackRook     : Piece = {symbol : "♜", name: "Black Rook", type: "Rook", isWhite : false};
export const blackKnight   : Piece = {symbol : "♞", name: "Black Knight", type: "Knight", isWhite : false};
export const blackBishop   : Piece = {symbol : "♝", name: "Black Bishop", type: "Bishop", isWhite : false};