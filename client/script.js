/*
 * @Author: Quentin Tonneau 
 * @Date: 2018-03-19 13:23:50 
 * @Last Modified by: Quentin Tonneau
 * @Last Modified time: 2018-03-19 15:20:20
 */

/**
 * Fonction principale executée après le chargement de la page
 * Initialise l'échiquier en fonction de la variable "echiquier"
 */
function init() {
    const CHESSBOARD_LENGTH = 8;

    for(var col = 0; col < CHESSBOARD_LENGTH; col++){
        for(var line = 0; line < CHESSBOARD_LENGTH; line++){
            var square = boardJSON.board[col][7-line]; // HTML Table lines are inversed
            var squareView = document.getElementsByTagName("tr")[line+1].getElementsByTagName("td")[col];
            if(square.isEmpty) {
                squareView.innerHTML = "";
            } else {
                squareView.innerHTML = square.piece.symbol;
            }
        }
	}
	
	const whiteColor = "#03a9f4"
	const blackColor = "#f44336"

	colorTurn(whiteColor, blackColor);
	colorPiece(whiteColor, blackColor);
	modal();
}

function colorTurn(whiteColor, blackColor) {
	const turn = document.getElementsByClassName("colorTurn turn")[0];
	const color = document.getElementsByClassName("colorTurn color")[0];
	
	if (color.innerText === "BLACK") {
		color.style.color = blackColor;
		turn.style.color = blackColor;
	} else if(color.innerText  === "WHITE") {
		color.style.color = whiteColor;
		turn.style.color = whiteColor;
	}
}

function colorPiece(whiteColor, blackColor) {
	const chessCase = document.getElementsByTagName("td");

	for (let i = 0 ; i < chessCase.length ; i++) {
		switch(chessCase[i].innerText) {
			// WHITE COLOR
			case "♙":
				chessCase[i].style.color = whiteColor;
			break;
			case "♔":
				chessCase[i].style.color = whiteColor;
			break;
			case "♕":
				chessCase[i].style.color = whiteColor;
			break;
			case "♖":
				chessCase[i].style.color = whiteColor;
			break;
			case "♘":
				chessCase[i].style.color = whiteColor;
			break;
			case "♗":
				chessCase[i].style.color = whiteColor;
			break;
			// BLACK COLOR
			case "♟":
				chessCase[i].style.color = blackColor;
			break;
			case "♚":
				chessCase[i].style.color = blackColor;
			break;
			case "♛":
				chessCase[i].style.color = blackColor;
			break;
			case "♜":
				chessCase[i].style.color = blackColor;
			break;
			case "♞":
				chessCase[i].style.color = blackColor;
			break;
			case "♝":
				chessCase[i].style.color = blackColor;
			break;
		}
	}
}

function modal() {
	modal = document.getElementsByClassName("modal")[0];
	checkbox = document.getElementById("checkbox");

	checkbox.addEventListener("change", () => {
		modal.style.display = "none";
	});
}